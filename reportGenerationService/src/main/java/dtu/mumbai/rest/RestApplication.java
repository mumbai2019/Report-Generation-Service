package dtu.mumbai.rest;

import javax.ws.rs.core.Application;
import dtu.mumbai.reportGenerationService.InternalCommunication;
import javax.ws.rs.ApplicationPath;


@ApplicationPath("/") //Don't touch this
public class RestApplication extends Application {
	public static InternalCommunication com = new InternalCommunication();
	/**
	 * @author s160902
	 */
	public RestApplication(){
		com.startReciever();
	}
}
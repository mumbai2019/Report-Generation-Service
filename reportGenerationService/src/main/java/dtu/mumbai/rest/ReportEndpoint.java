package dtu.mumbai.rest;

import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONObject;
import dtu.mumbai.reportGenerationService.InternalCommunication;
import enums.EventEnums;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
/**
 * @author s164161
 * 
 * 
 * */
@Path("/report")
public class ReportEndpoint {
	InternalCommunication com = RestApplication.com;
	
	/**
	 * For testing purposes only!
	 * Tests that REST can is up and responding to simple messages.
	 */
	  @GET
	  @Produces("text/plain")
	  public Response doGet() {
	    return Response.ok("Welcome to the report rest-interface").build();
	  }
	  /**
	   * Returns a complete report of all transactions made with the given cpr.
	   * @author s160902
	   */
	  @POST
	  @Path("/report")
	  @Consumes("application/json")
	  @Produces("application/json")
	  public Response getReportCustomer(String input) {
		  try {
			  JSONObject json = new JSONObject();
			  json.put("event", EventEnums.REPORTINFOPROVIDE);
			  json.put("cpr", new JSONObject(input).get("cpr"));
			  com.getTransmitter().pushMessageTo(json.toString(), "manager");
			  
			  JSONObject response = new JSONObject();
			  response.put("report", com.getReport());
			  return Response
					  .status(Response.Status.OK)
					  .entity(response)
					  .type(MediaType.APPLICATION_JSON)
					  .build();
		  }catch(Exception e) {
			  return Response
					  .status(Response.Status.CONFLICT)
					  .type(MediaType.APPLICATION_JSON)
					  .build();
		  }
		  
	  }
}
package dtu.mumbai.rabbitmq;

import org.json.JSONObject;

import dtu.mumbai.reportGenerationService.InternalCommunication;
import enums.EventEnums;


/**
 * ConsumerImplementation handles the messages from the receiver.
 * @author s160902
 *
 */
public class ConsumerImplementation {
	InternalCommunication com;
	public ConsumerImplementation(InternalCommunication com){
		this.com = com;
	}
	/**
	 * Handles the message given to it, based on the enum it contains.
	 * The message must be in JSON format and must contain an EventEnum at the key "event".
	 * @author s160902
	 * @param String message
	 */
	public void consumeMessage(String message) {
		System.out.println("Report Consumer recieved message: " + message);
		
		JSONObject jsonInput = new JSONObject(message);
		
		EventEnums enumEvent = EventEnums.valueOf(jsonInput.getString("event"));
		enumEvent.handleEvent(com, jsonInput);
	}
}

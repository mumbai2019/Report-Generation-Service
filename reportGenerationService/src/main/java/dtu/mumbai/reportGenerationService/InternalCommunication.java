package dtu.mumbai.reportGenerationService;

import dtu.mumbai.rabbitmq.ConsumerImplementation;
import dtu.mumbai.rabbitmq.Receiver;
import dtu.mumbai.rabbitmq.Transmitter;

/**
 * Contains functions needed for communicating internally between RabbitMQ and rest.
 * @author s160902
 */
public class InternalCommunication {
	private String report = null;
	
	private ConsumerImplementation consume = new ConsumerImplementation(this);	
	private Transmitter trans = new Transmitter();
	/**
	 * Runs the receiver.
	 * @author s160902
	 */
	public void startReciever() {
		Receiver recv = new Receiver(consume);
		recv.run();
	}
	/**
	 * Sets the report field and notifies the other waiting thread about the update.
	 * @author s160902
	 * @param report
	 */
	public synchronized void setReport(String report) {
		this.report = report;
		notify();
	}
	/**
	 * Attempts to get the report, waits for it if it's not there yet.
	 * @author s160902
	 * @return report
	 */
	public synchronized String getReport() {
		while(report == null) {
			try {
				wait();
			} catch (InterruptedException e) {}
		}
		String response = report;
		report = null;
		return response;
	}
	/**
	 * Returns the transmitter.
	 * @author s160902
	 * @return transmitter
	 */
	public Transmitter getTransmitter(){
		return trans;
	}
}

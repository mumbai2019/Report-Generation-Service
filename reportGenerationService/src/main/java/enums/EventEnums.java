package enums;

import org.json.JSONObject;

import dtu.mumbai.reportGenerationService.InternalCommunication;

public enum EventEnums 
{
	/**
	 * Creates a transaction report from the information in the received JSON.
	 * @author s160902
	 */
    REPORTINFO{
		@Override
		public void handleEvent(InternalCommunication com, JSONObject message) {
			message.remove("event");
			String report = "";
			for(String key : message.keySet()) {
				if(key.contains("token")) {
					report += message.getString(key)+"  ";
				}else if(key.contains("refunded")) {
					report += message.getBoolean(key)+" \n";
				}else {
					report += "  ";					
				}
			}
			com.setReport(report);
		}
    },
    /**
     * Used when transmitting messages only.
     */
    REPORTINFOPROVIDE{
		@Override
		public void handleEvent(InternalCommunication com, JSONObject message) {}
    	
    };
	public abstract void handleEvent(InternalCommunication com, JSONObject message);
}
#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)

@tag
Feature: Receive reports of transactions

  @tag1
  Scenario: Save reports after transaction
    Given A transaction that has been requested
    When the transactions is performed successfully
    Then I store the transaction in the DB

  @tag2
  Scenario: A user requests his/her previous transactions
    Given A user who has done transactions
    When He/she requests a weekly report
    Then I return a list of his/her transactions in the previous week